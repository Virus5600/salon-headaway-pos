﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ecommercePOS.Converter
{
    class MultiTotalConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
			foreach (var v in values)
				System.Diagnostics.Debug.WriteLine("V: " + v);
			//throw new NotImplementedException();
            //System.Diagnostics.Debug.WriteLine(values[0]);
            //System.Diagnostics.Debug.WriteLine(values[1]);

            double result = System.Convert.ToDouble(values[0]) * System.Convert.ToDouble(values[1]);
            String res = String.Format("{0:#.00}", System.Convert.ToDecimal(result.ToString()));
            return res;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
