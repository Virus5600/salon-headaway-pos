﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ecommercePOS.Converter
{
    public class TotalConverter : IValueConverter
    {
      

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string parameterString = parameter as string;

            if (!string.IsNullOrEmpty(parameterString))
            {
                string[] parameters = parameterString.Split(new char[] { '|' });
                // Now do something with the parameters
                Debug.WriteLine(parameters[0]);
                Debug.WriteLine(parameters[1]);
                Double qty  = System.Convert.ToDouble(parameters[0]);
                Double price = System.Convert.ToDouble(parameters[1]);

                return qty*price;
            }
            return 0;
            //throw new NotImplementedException();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        
    }
}
