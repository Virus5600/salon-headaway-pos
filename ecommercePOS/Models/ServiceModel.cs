﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecommercePOS.Models
{
	class ServiceModel
	{
		public class RootObject
		{
			public string result { get; set; }
			public List<ServiceVariation> services { get; set; }
		}
	}
}