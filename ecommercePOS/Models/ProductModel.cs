﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecommercePOS.Models
{
	class ProductModel
	{
		public class RootObject
		{
			public string result { get; set; }
			public List<Product> products { get; set; }
		}
	}
}