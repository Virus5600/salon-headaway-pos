﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ecommercePOS.Models
{
	public class Product : ObservableObject
	{
		public int id { get; set; }
		public string product_name { get; set; }
		public double price { get; set; }
		public string description { get; set; }
		public int in_stock { get; set;  }
		public int status { get; set; }

		private int _quantity;
		public int quantity
		{
			get { return _quantity; }
			set { SetProperty(ref _quantity, value); }
		}

		override public string ToString()
		{
			return "{id: " + this.id + ", product_name: \"" + this.product_name + "\", price: " + this.price + ", description: \"" + this.description + "\", in_stock: " + this.in_stock + ", status: \"" + this.status + "\", quantity: " + this._quantity + "}";
		}
	}
}
