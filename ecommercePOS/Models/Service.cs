﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ecommercePOS.Models
{
	public class Service : ObservableObject
	{
		public int id { get; set; }
		public int service_category_id{ get; set; }
		public string service_name { get; set; }
		public string description { get; set;  }

		private int _quantity;
		public int quantity
		{
			get { return _quantity; }
			set { SetProperty(ref _quantity, value); }
		}

		override public string ToString()
		{
			return "{id: " + this.id + ", service_name: \"" + this.service_name + "\", description: \"" + this.description + "\"}";
		}
	}
}
