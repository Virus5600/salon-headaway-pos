﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecommercePOS.Models
{
	public class ServiceVariation : ObservableObject
	{
		public int id { get; set; }
		public int service_id { get; set; }
		public string variation_name { get; set; }
		private decimal _price_min;
		public decimal price_min
		{
			get { return _price_min; }
			set { SetProperty(ref _price_min, value); }
		}
		public decimal price_max { get; set; }
		public string description { get; set; }
		public bool is_price_max_and_up { get; set; }

		public Service service { get; set; }

		override public string ToString()
		{
			return "{id: " + this.id + ", service_id: \"" + this.service_id+ "\", variation_name: " + this.variation_name + ", price: " + this.price_min +  ", description: \"" + this.description + "}";
		}
	}
}