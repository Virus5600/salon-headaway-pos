﻿using ecommercePOS.Models;
using ecommercePOS.Properties;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;

namespace ecommercePOS
{
	class DBClass
	{
		public static string TestServer()
		{
			string resultContent = "";
			using (var client = new HttpClient())
			{
				System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
				ServicePointManager.ServerCertificateValidationCallback = new
				RemoteCertificateValidationCallback
				(
					delegate { return true; }
				);
				client.BaseAddress = new Uri(Settings.Default["SERVER_HOST"].ToString());
				var result = client.GetAsync("/api/test-server").Result;
				resultContent = result.Content.ReadAsStringAsync().Result;
				return resultContent;
			}

		}
		// PRODUCT
		public static string FetchProduct()
		{
			string resultContent = "";
			using (var client = new HttpClient())
			{
				System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
				ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(
					delegate { return true; }
				);
				client.BaseAddress = new Uri(Settings.Default["SERVER_HOST"].ToString());
				var result = client.GetAsync("/api/fetch-products").Result;
				resultContent = result.Content.ReadAsStringAsync().Result;
				return resultContent;
			}
		}
		public static string SearchProduct(string search)
		{
			string resultContent = "";
			using (var client = new HttpClient())
			{
				System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
				ServicePointManager.ServerCertificateValidationCallback = new
				RemoteCertificateValidationCallback
				(
					delegate { return true; }
				);
				client.BaseAddress = new Uri(Settings.Default["SERVER_HOST"].ToString());
				var result = client.GetAsync("/api/fetch-products?search=" + search).Result;
				resultContent = result.Content.ReadAsStringAsync().Result;
				return resultContent;
			}

		}

		// SERVICE
		public static string FetchService()
		{
			string resultContent = "";
			using (var client = new HttpClient())
			{
				System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
				ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(
					delegate { return true; }
				);
				client.BaseAddress = new Uri(Settings.Default["SERVER_HOST"].ToString());
				var result = client.GetAsync("/api/fetch-services").Result;
				resultContent = result.Content.ReadAsStringAsync().Result;
				return resultContent;
			}
		}
		public static string SearchService(string search)
		{
			string resultContent = "";
			using (var client = new HttpClient())
			{
				System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
				ServicePointManager.ServerCertificateValidationCallback = new
				RemoteCertificateValidationCallback
				(
					delegate { return true; }
				);
				client.BaseAddress = new Uri(Settings.Default["SERVER_HOST"].ToString());
				var result = client.GetAsync("/api/fetch-services?search=" + search).Result;
				resultContent = result.Content.ReadAsStringAsync().Result;
				return resultContent;
			}
		}

		public static string fetchProductBarcode(string search)
		{
			string resultContent = "";
			using (var client = new HttpClient())
			{
				System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
				ServicePointManager.ServerCertificateValidationCallback = new
				RemoteCertificateValidationCallback
				(
					delegate { return true; }
				);
				client.BaseAddress = new Uri(Settings.Default["SERVER_HOST"].ToString());
				Debug.WriteLine("/api/kiosk-fetch-products-barcode?search=" + search);
				var result = client.GetAsync("/api/kiosk-fetch-products-barcode?search=" + search).Result;
				resultContent = result.Content.ReadAsStringAsync().Result;
				return resultContent;
			}

		}

		public static bool SubmitOrder(List<Product> listProd, List<ServiceVariation> listServ, Dictionary<string, decimal> payment)
		{
			string resultContent = "";
			using (var client = new HttpClient())
			{
				System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
				ServicePointManager.ServerCertificateValidationCallback = new
				RemoteCertificateValidationCallback
				(
					delegate { return true; }
				);
				client.BaseAddress = new Uri(Settings.Default["SERVER_HOST"].ToString());
				var prod = JsonConvert.SerializeObject(listProd);
				var serv = JsonConvert.SerializeObject(listServ);

				var d = new Dictionary<string, string>();
				d.Add("prod", prod);
				d.Add("serv", serv);
				d.Add("paid", JsonConvert.SerializeObject(payment));

				var ps = JsonConvert.SerializeObject(d);
				var content = new StringContent(ps, Encoding.UTF8, "application/json");

				var result = client.PostAsync("/api/submit-payment", content).Result;
				resultContent = result.Content.ReadAsStringAsync().Result;
				var settings = new JsonSerializerSettings
				{
					NullValueHandling = NullValueHandling.Ignore,
					MissingMemberHandling = MissingMemberHandling.Ignore
				};
				Debug.WriteLine(resultContent);
				var val = JsonConvert.DeserializeObject<ServerTestModel.RootObject>(resultContent, settings);

				return val.success;
			}
		}
	}
}