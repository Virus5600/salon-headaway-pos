﻿using ecommercePOS.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ecommercePOS.Forms
{
	/// <summary>
	/// Interaction logic for ProductList.xaml
	/// </summary>
	public partial class ProductList : Window
	{
		//public List<Product> productList { get; set; }
		private MainWindow mw;
		private ObservableCollection<Product> productList;
		Product p = new Product();

		public Product prod { get { return p; } }

		public ProductList(MainWindow mw)
		{
			this.mw = mw;
			this.Owner = mw;
			InitializeComponent();
			productList = new ObservableCollection<Product>();
			listView.ItemsSource = productList;
		}

		private void fetchProduct()
		{
			try
			{				
				var res = DBClass.FetchProduct();
				var settings = new JsonSerializerSettings
				{
					NullValueHandling = NullValueHandling.Ignore,
					MissingMemberHandling = MissingMemberHandling.Ignore
				};

				var val = JsonConvert.DeserializeObject<ProductModel.RootObject>(res, settings);
				foreach (Product p in val.products)
					productList.Add(p);
			}
			catch (Exception ex)
			{				
				MessageBox.Show("SERVER ERROR " + ex.ToString(), "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}
		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			fetchProduct();
		}

		private void search(string search)
		{
			productList.Clear();
			try
			{
				var res = DBClass.SearchProduct(search);
				var settings = new JsonSerializerSettings
				{
					NullValueHandling = NullValueHandling.Ignore,
					MissingMemberHandling = MissingMemberHandling.Ignore
				};
				var val = JsonConvert.DeserializeObject<ProductModel.RootObject>(res, settings);
				foreach (Product p in val.products)
				{
					productList.Add(p);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("SERVER ERROR " , "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}
		private void btnSearch_Click(object sender, RoutedEventArgs e)
		{
			search(txtSearch.Text);

		}

		private void listView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			selectItem();
		}
		private void selectItem()
		{
			Product p = new Product();
			p = (Product)listView.SelectedItem;
			if (p != null)
			{
				if (p.quantity == 0)
				{
					p.quantity = 1;
					mw.productList.Add(p);
					this.DialogResult = true;
				}
			}
			else
				this.DialogResult = false;
		}
	}
}