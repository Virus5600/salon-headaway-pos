﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ecommercePOS.Forms
{
    /// <summary>
    /// Interaction logic for ChangeQty.xaml
    /// </summary>
    public partial class ChangeQty : Window
    {
        private MainWindow mainWindow;
        int qty = 0;
        private string name;
        public int changeQty
        {
            get { return qty; }
        }

        public ChangeQty(MainWindow mainWindow, string name)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            this.Owner = mainWindow;
            this.name = name;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lblName.Content = "Change quantity of " + this.name;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            qty = Convert.ToInt32(txtChange.Text);
            this.DialogResult = true;
        }
    }
}
