﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ecommercePOS.Forms
{
    /// <summary>
    /// Interaction logic for ChangePrice.xaml
    /// </summary>
    public partial class ChangePrice : Window
    {
        private MainWindow mainWindow;
		private decimal price = 0;
		private string name;
        public decimal changePrice
        {
            get { return price; }
        }

        public ChangePrice(MainWindow mainWindow, string name, decimal price)
        {
            InitializeComponent();
            this.mainWindow = mainWindow;
            this.Owner = mainWindow;
            this.name = name;
            this.price = price;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            lblName.Content = "Change Price of " + this.name;
            txtOriginalPrice.Text = String.Format("{0:#.00}", System.Convert.ToDecimal(this.price));
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            price = Convert.ToDecimal(txtChange.Text);
            this.DialogResult = true;
        }
    }
}
