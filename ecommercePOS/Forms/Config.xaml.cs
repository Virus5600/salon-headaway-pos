﻿using ecommercePOS.Models;
using ecommercePOS.Properties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ecommercePOS.Forms
{
	/// <summary>
	/// Interaction logic for Config.xaml
	/// </summary>
	public partial class Config : Window
	{
		private void connect()
		{
			MainWindow win1 = new MainWindow();
			this.Close();
			win1.Show();
		}
		private void btnConnect_Click(object sender, RoutedEventArgs e)
		{
		 
			connect();
		}
		private void Window_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Escape)
			{
				if (MessageBox.Show("Are you sure you want to close this application?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
				{
					Close();
				}

			}
		}
		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			Settings.Default.Save();
		}
		private void Window_ContentRendered(object sender, EventArgs e) {}
		private void Window_Activated(object sender, EventArgs e) {}
		private void Window_Closed(object sender, EventArgs e) {}

		public Config()
		{
			InitializeComponent();
			txtServer.Text = Settings.Default["SERVER_HOST"].ToString();
		}

		private void ConnectToServer()
		{
			//MesssageBox.Show("v2");

			try
			{
				btnConnect.IsEnabled = false;
				Settings.Default["SERVER_HOST"] = txtServer.Text;
				Settings.Default.Save();


				var res = DBClass.TestServer();
				var testModel = JsonConvert.DeserializeObject<ServerTestModel.RootObject>(res);

				if (testModel.success)
				{
					MainWindow win1 = new MainWindow();
					this.Close();
					win1.Show();

				}
				else
				{
					btnConnect.IsEnabled = true;
					MessageBox.Show("Not Okay", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
				}
			}
			catch (Exception ex)
			{
				btnConnect.IsEnabled = true;
				MessageBox.Show("SERVER ERROR ", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
			
			}
		}

		private void button_Click(object sender, RoutedEventArgs e)
		{
			checkServer();
		}
		private void checkServer()
		{
			try
			{
				button.IsEnabled = false;
				Settings.Default["SERVER_HOST"] = txtServer.Text;
				Settings.Default.Save();

			   
				var res = DBClass.TestServer();
				if (res == "1")
				{
					button.IsEnabled = true;
					// comboBox.IsEnabled = true;
					btnConnect.IsEnabled = true;
					// status.IsEnabled = true;
					//fetchDepartments();
				}
				else
				{
					button.IsEnabled = true;
					MessageBox.Show("Not Okay", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
				}
			}
			catch (Exception ex)
			{
				button.IsEnabled = true;
				MessageBox.Show("SERVER ERROR ", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

	}
}
