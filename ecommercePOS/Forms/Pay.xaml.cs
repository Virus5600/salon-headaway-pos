﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ecommercePOS.Forms
{
    /// <summary>
    /// Interaction logic for Pay.xaml
    /// </summary>
    public partial class Pay : Window
    {
		private String _total = "";
		private decimal _paid = 0;

		public decimal total { get { return Convert.ToDecimal(this._total); } }
		public decimal paid { get { return _paid; } }

        public Pay(MainWindow mw,String total)
        {
            this._total = total;
            this.Owner = mw;
            InitializeComponent();
        }
        decimal change = 0 ;
        public decimal changeAmt
        {
            get { return change; }
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            if(Convert.ToDecimal(txtPay.Text)>= Convert.ToDecimal(txtTotal.Text))
            {
                change = System.Convert.ToDecimal(txtChange.Text);
                this.DialogResult = true;
            }
            
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtTotal.Text = _total;
        }

        private void txtPay_TextChanged(object sender, TextChangedEventArgs e)
        {
            try {
                if (txtChange != null)
                {
                    Decimal tot = Convert.ToDecimal(txtPay.Text) - Convert.ToDecimal(txtTotal.Text);

                    if (tot > 0)
                    {
                        txtChange.Text = String.Format("{0:#.0000}", tot.ToString());
                    }
                    else
                    {
                        txtChange.Text = "0";
                    }
                }
            }
            catch(Exception ex){}
        }
    }
}
