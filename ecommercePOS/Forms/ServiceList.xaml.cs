﻿using ecommercePOS.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ecommercePOS.Forms
{
	/// <summary>
	/// Interaction logic for ServiceList.xaml
	/// </summary>
	public partial class ServiceList : Window
	{
		//public List<Service> serviceList { get; set; }
		private MainWindow mw;
		private ObservableCollection<ServiceVariation> serviceList;
		ServiceVariation sv = new ServiceVariation();

		public ServiceVariation serviceVariation { get { return sv; } }

		public ServiceList(MainWindow mw)
		{
			this.mw = mw;
			this.Owner = mw;
			InitializeComponent();
			serviceList = new ObservableCollection<ServiceVariation>();
			listView.ItemsSource = serviceList;
		}

		private void fetchService()
		{
			try
			{				
				var res = DBClass.FetchService();
				var settings = new JsonSerializerSettings
				{
					NullValueHandling = NullValueHandling.Ignore,
					MissingMemberHandling = MissingMemberHandling.Ignore
				};

				var val = JsonConvert.DeserializeObject<ServiceModel.RootObject>(res, settings);
				foreach (ServiceVariation s in val.services)
				{
					serviceList.Add(s);
				}
			}
			catch (Exception ex)
			{				
				MessageBox.Show("SERVER ERROR " + ex.ToString(), "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}
		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			fetchService();
		}

		private void search(string search)
		{
			serviceList.Clear();
			try
			{
				var res = DBClass.SearchService(search);
				var settings = new JsonSerializerSettings
				{
					NullValueHandling = NullValueHandling.Ignore,
					MissingMemberHandling = MissingMemberHandling.Ignore
				};
				var val = JsonConvert.DeserializeObject<ServiceModel.RootObject>(res, settings);
				foreach (ServiceVariation s in val.services)
				{
					serviceList.Add(s);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("SERVER ERROR " , "Alert", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}
		private void btnSearch_Click(object sender, RoutedEventArgs e)
		{
			search(txtSearch.Text);

		}

		private void listView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			selectService();
		}
		private void selectService()
		{
			ServiceVariation sv = new ServiceVariation();
			sv = (ServiceVariation)listView.SelectedItem;

			if (sv != null)
			{
				if (sv.service.quantity == 0)
				{
					sv.service.quantity = 1;
					mw.serviceList.Add(sv);
					this.DialogResult = true;
				}
			}
			else
				this.DialogResult = false;
		}
	}
}