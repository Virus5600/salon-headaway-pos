﻿using ecommercePOS.Forms;
using ecommercePOS.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
//using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.IO;
using ecommercePOS.Properties;
using Newtonsoft.Json;
using System.Drawing.Printing;
using System.Drawing;

namespace ecommercePOS
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		int Offset = 10;
		int startX = 0;
		int startY = 0;
		decimal change = 0;
		public ObservableCollection<ServiceVariation> serviceList;
		public ObservableCollection<Product> productList;

		public MainWindow()
		{
			InitializeComponent();
			serviceList = new ObservableCollection<ServiceVariation>();
			productList = new ObservableCollection<Product>();
			listViewService.ItemsSource = serviceList;
			listView.ItemsSource = productList;
			DataContext = this;

			//Product p = new Product();
			//p.id = 99999;
			//p.product_name = "asdsadsaa";
			//productList.Add(p);
			//productList.Add(p);
			//productList.Add(p);
		}
		//public MainWindow(Product p ) : this {}

		static public void AddProduct(Product p)
		{

			//Product x = new Product();
			//x.id = 99999;
			//x.product_name = "asdsadsaa";
			//productList.Add(x);
			//Debug.WriteLine(productList.Count + "  ");
			//Debug.WriteLine(x.product_name);
		}

		private void Button_Click(object sender, RoutedEventArgs e)
		{
			//txtTotal.Text = "11112";
			pay();
		}

		private void pay()
		{
			var p = new Pay(this, txtTotal.Text);
			p.Owner = this.Owner;
			if (p.ShowDialog() == true)
			{
				change = p.changeAmt;
				List<Product> plist = new List<Product>();
				List<ServiceVariation> slist = new List<ServiceVariation>();
				foreach (Product x in productList)
					plist.Add(x);
				foreach (ServiceVariation x in serviceList)
					slist.Add(x);

				var payment = new Dictionary<string, decimal>();
				payment.Add("total", p.total);
				payment.Add("paid", p.paid);

				if (DBClass.SubmitOrder(plist, slist, payment) == true)
				{
					MessageBox.Show("done");
				}
				Print();
				productList.Clear();
				serviceList.Clear();
				txtTotal.Text = "0.00";
			}
		}

		private void Window_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Return)
			{
				var res = DBClass.fetchProductBarcode(txtBarCode.Text);
				var settings = new JsonSerializerSettings {
					NullValueHandling = NullValueHandling.Ignore,
					MissingMemberHandling = MissingMemberHandling.Ignore
				};

				var val = JsonConvert.DeserializeObject<ProductModel.RootObject>(res, settings);
				Product p = val.products.First();
				Debug.WriteLine(p);
				if (p.quantity == 0)
				{
					p.quantity = 1;
				}
				productList.Add(p);
				calculateTotal();

				txtBarCode.Text = "";
			}
			if (e.Key == Key.F4)
			{
				if (productList.Count > 0)
				{
					pay();
				}
			}
			if (e.Key == Key.F1)
			{
				searchItem();
			}
			if (e.Key == Key.Up)
			{
				moveUpDown(1);
			}
			if (e.Key == Key.Down)
			{
				moveUpDown(0);
			}
		}
		private void moveUpDown(int upDown)
		{

			if (upDown == 1)
			{

			}
			else
			{
				int selected = listView.SelectedIndex;
				if (selected < productList.Count())
				{
					//listView.SelectedIndex = selected + 1;
				}
			}
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			txtBarCode.Focusable = true;
			Keyboard.Focus(txtBarCode);
		}
		public void Print()
		{
			var doc = new PrintDocument();
			doc.PrintPage += new PrintPageEventHandler(ProvideContent);
			doc.Print();
		}
		private void truncateStr(Graphics graphics, string str)
		{
			int len = str.Length;

			string title = str.Substring(0, str.Length);
			graphics.DrawString(title, new Font("Courier New", 8), new SolidBrush(Color.Black), startX, startY + Offset);
			Offset = Offset + 10;

			if (str.Length > 27)
			{
				string sb = str.Substring(28, str.Length - 28);
				truncateStr(graphics, sb);
			}
		}

		public void ProvideContent(object sender, PrintPageEventArgs e)
		{
			Graphics graphics = e.Graphics;
			Font font = new Font("Courier New", 8);

			float fontHeight = font.GetHeight();

			startX = 0;
			startY = 0;
			Offset = 10;

			//e.PageSettings.PaperSize.Width = 50;

			graphics.DrawString("Headaway Salon", new Font("Courier New", 8),
								new SolidBrush(Color.Black), startX, startY + Offset);
			Offset = Offset + 10;

			graphics.DrawString("4027 Calamba,", new Font("Courier New", 8),
							   new SolidBrush(Color.Black), startX, startY + Offset);
			Offset = Offset + 10;
			graphics.DrawString("Philippines", new Font("Courier New", 8),
							   new SolidBrush(Color.Black), startX, startY + Offset);
			Offset = Offset + 10;
			graphics.DrawString("+(639) 61533-3163", new Font("Courier New", 8),
							   new SolidBrush(Color.Black), startX, startY + Offset);
			Offset = Offset + 10;

			graphics.DrawString("" + DateTime.Now.Date.ToString(), new Font("Courier New", 8),
							   new SolidBrush(Color.Black), startX, startY + Offset);
			Offset = Offset + 10;

			graphics.DrawString("SALES INVOICE", new Font("Courier New", 10),
							   new SolidBrush(Color.Black), startX, startY + Offset);
			Offset = Offset + 20;

			graphics.DrawString("-------------------------------------", new Font("Courier New", 10),
							   new SolidBrush(Color.Black), startX, startY + Offset);
			Offset = Offset + 20;

			foreach (Product p in productList)
			{
				double result = System.Convert.ToDouble(p.price) * System.Convert.ToDouble(p.quantity);
				String res = String.Format("{0:#.00}", System.Convert.ToDecimal(result.ToString()));

				truncateStr(graphics, "" + p.product_name + " P" + p.price+ " x " + p.quantity + " P" + res);
				Offset = Offset + 10;

			}

			Offset = Offset + 10;
			String underLine = "------------------------------------------";

			graphics.DrawString(underLine, new Font("Courier New", 8),
						new SolidBrush(Color.Black), startX, startY + Offset);
			Offset = Offset + 10;

			String Grosstotal = "Subtotal";
			graphics.DrawString(Grosstotal, new Font("Courier New", 8),
						new SolidBrush(Color.Black), startX, startY + Offset);

			String GrosstotalValue = "P" + String.Format("{0:#.0000}", System.Convert.ToDecimal(txtTotal.Text));
			graphics.DrawString(GrosstotalValue, new Font("Courier New", 8),
						new SolidBrush(Color.Black), startX + 100, startY + Offset);

			Offset = Offset + 10;

			String discount = "Discount";
			graphics.DrawString(discount, new Font("Courier New", 8),
						new SolidBrush(Color.Black), startX, startY + Offset);

			String discountValue = "P" + String.Format("{0:#.0000}", System.Convert.ToDecimal("0"));
			graphics.DrawString(discountValue, new Font("Courier New", 8),
						new SolidBrush(Color.Black), startX + 100, startY + Offset);

			Offset = Offset + 10;

			String vat = "VAT";
			graphics.DrawString(vat, new Font("Courier New", 8),
						new SolidBrush(Color.Black), startX, startY + Offset);
			double gross = System.Convert.ToDouble(txtTotal.Text);
			double vt = gross - (gross / 1.12);
			String vatValue = "P" + String.Format("{0:#.0000}", vt);
			graphics.DrawString(vatValue, new Font("Courier New", 8),
						new SolidBrush(Color.Black), startX + 100, startY + Offset);

			Offset = Offset + 10;

			String totalIncludingVat = "Total ";
			graphics.DrawString(totalIncludingVat, new Font("Courier New", 8),
						new SolidBrush(Color.Black), startX, startY + Offset);

			String totVat = "P" + String.Format("{0:#.0000}", System.Convert.ToDecimal(txtTotal.Text));
			graphics.DrawString(totVat, new Font("Courier New", 8),
						new SolidBrush(Color.Black), startX + 100, startY + Offset);

			Offset = Offset + 10;


			String itemCount = "No. Of Items " + productList.Count();
			graphics.DrawString(itemCount, new Font("Courier New", 8),
						new SolidBrush(Color.Black), startX, startY + Offset);

			Offset = Offset + 10;


			underLine = "------------------------------------------";
			graphics.DrawString(underLine, new Font("Courier New", 8),
						new SolidBrush(Color.Black), startX, startY + Offset);
			Offset = Offset + 10;


		}

		private void numpadclick(object sender, RoutedEventArgs e)
		{
			var button = sender as Button;
			var tag = button.Tag.ToString();

			txtBarCode.CaretIndex = txtBarCode.Text.Length;
			var rect = txtBarCode.GetRectFromCharacterIndex(txtBarCode.CaretIndex);
			txtBarCode.ScrollToHorizontalOffset(rect.Right);
			switch (tag)
			{
				case "1":
					txtBarCode.AppendText("1");
					break;
				case "2":
					txtBarCode.AppendText("2");
					break;
				case "3":
					txtBarCode.AppendText("3");
					break;
				case "4":
					txtBarCode.AppendText("4");
					break;
				case "5":
					txtBarCode.AppendText("5");
					break;
				case "6":
					txtBarCode.AppendText("6");
					break;
				case "7":
					txtBarCode.AppendText("7");
					break;
				case "8":
					txtBarCode.AppendText("8");
					break;
				case "9":
					txtBarCode.AppendText("9");
					break;
				case "0":
					txtBarCode.AppendText("0");
					break;
				case "c":
					txtBarCode.Text = "";
					break;
				case ".":
					txtBarCode.AppendText(".");
					break;
				default:
					break;
			}
			Keyboard.Focus(txtBarCode);

		}

		// SERVICES
		private void searchService()
		{
			var serviceListForm = new ServiceList(this);
			serviceListForm.Owner = this.Owner;

			if (serviceListForm.ShowDialog() == true)
			{
				calculateTotal();
			}

		}
		private void searchServiceClick(object sender, RoutedEventArgs e)
		{
			searchService();
		}
		private void changeServiceQuantityClick(object sender, RoutedEventArgs e)
		{
			try
			{
				changeQuantity(false);
			}
			catch (Exception ex)
			{
				MessageBox.Show("Please Select a Service");
			}

		}
		private void changePrice()
		{
			ServiceVariation sv = new ServiceVariation();
			sv = (ServiceVariation)listViewService.SelectedItem;
			if (listViewService.SelectedItem != null)
			{
				var serviceListForm = new ChangePrice(this, sv.service.service_name + (sv.variation_name == null ? "" : " - " + sv.variation_name), sv.price_min);
				serviceListForm.Owner = this.Owner;

				if (serviceListForm.ShowDialog() == true)
				{
					var item = serviceList.FirstOrDefault(i => i.id == sv.id);
					if (item != null)
					{
						item.price_min = serviceListForm.changePrice;
					}
				}
				calculateTotal();
			}
		}
		private void changePriceClick(object sender, RoutedEventArgs e)
		{
			try
			{
				changePrice();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Please Select a Service");
			}
		}
		private void removeServiceClick(object sender, RoutedEventArgs e)
		{
			try
			{
				removeService();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Please Select Service");
			}
		}
		private void removeService()
		{
			ServiceVariation sv = new ServiceVariation();
			sv = (ServiceVariation)listViewService.SelectedItem;
			if (listViewService.SelectedItem != null)
			{
				var Result = MessageBox.Show("Remove Service", "Would you like to remove this?", MessageBoxButton.YesNo, MessageBoxImage.Question);
				if (Result == MessageBoxResult.Yes)
				{
					var item = serviceList.FirstOrDefault(i => i.id == sv.id);
					if (item != null)
					{
						serviceList.Remove(item);
					}
				}
				else if (Result == MessageBoxResult.No)
				{
					MessageBox.Show("You Reserved this seat");
				}
				calculateTotal();
			}
		}

		// PRODUCTS
		private void searchItem()
		{
			var productListForm = new ProductList(this);
			productListForm.Owner = this.Owner;
			productListForm.ShowDialog();
		}
		private void searchItemClick(object sender, RoutedEventArgs e)
		{
			searchItem();
		}
		private void changeProductQuantityClick(object sender, RoutedEventArgs e)
		{
			try
			{
				changeQuantity(true);
			}
			catch (Exception ex)
			{
				MessageBox.Show("Please Select a Product");
			}
			
		}
		private void removeProductClick(object sender, RoutedEventArgs e)
		{
			try
			{
				removeProduct();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Please Select Product");
			}
		}
		private void removeProduct()
		{
			Product pr = new Product();
			pr = (Product)listView.SelectedItem;
			if (listView.SelectedItem != null)
			{
				var Result = MessageBox.Show("Remove Product", "Would you like to remove this?", MessageBoxButton.YesNo, MessageBoxImage.Question);
				if (Result == MessageBoxResult.Yes)
				{
					var item = productList.FirstOrDefault(i => i.id == pr.id);
					if (item != null)
						productList.Remove(item);
				}
				else if (Result == MessageBoxResult.No)
				{
					MessageBox.Show("You Reserved this seat");
				}
			}
		}

		// GENERAL METHODS
		private void calculateTotal()
		{
			Double subtotal = 0;
			foreach (ServiceVariation s in serviceList)
			{
				subtotal += (Convert.ToDouble(s.price_min) * Convert.ToDouble(s.service.quantity));
			}
			txtTotal.Text = subtotal.ToString("F");
		}
		private void changeQuantity(bool isProduct)
		{
			Product p = null;
			ServiceVariation s = null;

			p = (Product)listView.SelectedItem;
			s = (ServiceVariation)listViewService.SelectedItem;

			Debug.WriteLine("Product: " + p);
			Debug.WriteLine("Service: " + s);

			if (listView.SelectedItem != null || listViewService != null)
			{
				if (isProduct)
				{
					var productListForm = new ChangeQty(this, p.product_name);
					productListForm.Owner = this.Owner;

					if (productListForm.ShowDialog() == true)
					{
						var item = productList.FirstOrDefault(i => i.id == p.id);
						if (item != null)
						{
							item.quantity = productListForm.changeQty;
						}
					}
				}
				else
				{
					var serviceListForm = new ChangeQty(this, s.service.service_name);
					serviceListForm.Owner = this.Owner;

					if (serviceListForm.ShowDialog() == true)
					{
						var item = serviceList.FirstOrDefault(i => i.id == s.id);
						if (item != null)
						{
							item.service.quantity = serviceListForm.changeQty;
						}
					}
				}
				calculateTotal();
			}
		}
	}
}